package fr.eql.ai110.NOK;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NokApplication {

	public static void main(String[] args) {
		SpringApplication.run(NokApplication.class, args);
	}

}
