package fr.eql.ai110.NOK.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.NOK.model.Category;
import fr.eql.ai110.NOK.model.Product;
import fr.eql.ai110.NOK.model.ProductPurchase;
import fr.eql.ai110.NOK.model.Purchase;
import fr.eql.ai110.NOK.model.DeliveryMode;
import fr.eql.ai110.NOK.model.PaymentMethod;
import fr.eql.ai110.NOK.repository.CategoryIDAO;
import fr.eql.ai110.NOK.repository.ProductIDAO;
import fr.eql.ai110.NOK.repository.ProductPurchaseIDAO;
import fr.eql.ai110.NOK.repository.PurchaseIDAO;
import fr.eql.ai110.NOK.repository.DeliveryModeIDAO;
import fr.eql.ai110.NOK.repository.PaymentMethodIDAO;
import fr.eql.ai110.NOK.service.CommandIBusiness;

@Service
public class CommandBusiness implements CommandIBusiness {

	@Autowired
	CategoryIDAO categoryDao;
	@Autowired
	ProductIDAO productDao;
	@Autowired
	DeliveryModeIDAO deliveryModeDao;
	@Autowired
	PurchaseIDAO purchaseDao;
	@Autowired
	ProductPurchaseIDAO productPurchaseDao;
	@Autowired
	PaymentMethodIDAO paymentMethodDao;
	
	@Override
	public Category getCategoryByName(String name) {
		return categoryDao.findByName(name);
	}

	@Override
	public List<Category> getAllCategories() {
		return categoryDao.findAll();
	}
	
	@Override
	public List<Product> getAllProducts() {
		return productDao.findAll();
	}

	@Override
	public List<Product> getProductsByCategory(Category category) {
		return productDao.findByCategory(category);
	} 
	
	@Override
	public List<DeliveryMode> getAllPurchaseMode(){
		return deliveryModeDao.findAll();
	}
	
	@Override
	public void addOnePurchase(Purchase purchase) {
		purchaseDao.save(purchase);
	}
	
	@Override
	public void addOneProductPurchase(ProductPurchase productPurchase) {
		productPurchaseDao.save(productPurchase);
	}
	
	@Override
	public List<PaymentMethod> getAllPaymentMethod(){
		return paymentMethodDao.findAll();
	}
	
}
