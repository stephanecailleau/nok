package fr.eql.ai110.NOK.service;

import java.util.List;


import fr.eql.ai110.NOK.model.Category;
import fr.eql.ai110.NOK.model.Product;
import fr.eql.ai110.NOK.model.ProductPurchase;
import fr.eql.ai110.NOK.model.Purchase;
import fr.eql.ai110.NOK.model.DeliveryMode;
import fr.eql.ai110.NOK.model.PaymentMethod;

public interface CommandIBusiness {

	List<Category> getAllCategories();
 	Category getCategoryByName(String name);
 	
	List<Product> getAllProducts();
	List<Product> getProductsByCategory(Category category);
	
	List<DeliveryMode> getAllPurchaseMode();
	
	void addOnePurchase(Purchase purchase);
	
	void addOneProductPurchase(ProductPurchase productPurchase);
	
	List<PaymentMethod> getAllPaymentMethod();
}
