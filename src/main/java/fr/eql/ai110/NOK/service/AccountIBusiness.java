package fr.eql.ai110.NOK.service;

import java.util.Set;

import fr.eql.ai110.NOK.model.Customer;

public interface AccountIBusiness {

	Customer connect(String email, String password);
	Set<Customer> getAllCustomers();

	
}
