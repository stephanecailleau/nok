package fr.eql.ai110.NOK.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eql.ai110.NOK.model.Customer;
import fr.eql.ai110.NOK.repository.CustomerIDAO;
import fr.eql.ai110.NOK.service.AccountIBusiness;

@Service
public class AccountBusiness implements AccountIBusiness{
	

	@Autowired
	private CustomerIDAO customerDao;
	
	@Override
	public Customer connect(String email, String password) {
		return customerDao.findByEmailAndPassword(email, password);
	}

	@Override
	public Set<Customer> getAllCustomers() {
		return (Set<Customer>) customerDao.findAll();
	}

	
}
