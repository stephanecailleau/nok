package fr.eql.ai110.NOK.repository;


import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.NOK.model.Customer;


public interface CustomerIDAO extends CrudRepository<Customer, Long> {

	Customer findByEmailAndPassword(String email, String password);
}
