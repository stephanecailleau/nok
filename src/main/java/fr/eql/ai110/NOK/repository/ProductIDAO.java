package fr.eql.ai110.NOK.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.NOK.model.Category;
import fr.eql.ai110.NOK.model.Product;

public interface ProductIDAO extends CrudRepository<Product, Long> {
	
	List<Product> findAll();
	List<Product> findByCategory(Category category);
	
}
