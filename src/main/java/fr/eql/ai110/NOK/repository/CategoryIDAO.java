package fr.eql.ai110.NOK.repository;



import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.NOK.model.Category;



public interface CategoryIDAO extends CrudRepository<Category, Long>{
	List<Category> findAll();
	Category findByName(String name);
}
