package fr.eql.ai110.NOK.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.NOK.model.PaymentMethod;



public interface PaymentMethodIDAO extends CrudRepository<PaymentMethod, Long>{

	List<PaymentMethod> findAll();
}
