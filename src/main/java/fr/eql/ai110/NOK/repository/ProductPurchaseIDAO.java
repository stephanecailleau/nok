package fr.eql.ai110.NOK.repository;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.NOK.model.ProductPurchase;

public interface ProductPurchaseIDAO extends CrudRepository<ProductPurchase, Long>{

	
}
