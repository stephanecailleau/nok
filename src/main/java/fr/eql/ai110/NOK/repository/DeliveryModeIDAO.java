package fr.eql.ai110.NOK.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.eql.ai110.NOK.model.DeliveryMode;

public interface DeliveryModeIDAO extends CrudRepository<DeliveryMode, Long>{
	
	List<DeliveryMode> findAll();
}
