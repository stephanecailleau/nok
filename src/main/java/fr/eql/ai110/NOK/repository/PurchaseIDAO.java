package fr.eql.ai110.NOK.repository;

import org.springframework.data.repository.CrudRepository;


import fr.eql.ai110.NOK.model.Purchase;

public interface PurchaseIDAO  extends CrudRepository<Purchase, Long> {
	
	
	
}
