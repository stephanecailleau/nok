package fr.eql.ai110.NOK.controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import fr.eql.ai110.NOK.model.Cart;
import fr.eql.ai110.NOK.model.Category;
import fr.eql.ai110.NOK.model.Customer;
import fr.eql.ai110.NOK.model.Product;
import fr.eql.ai110.NOK.model.ProductPurchase;
import fr.eql.ai110.NOK.model.Purchase;
import fr.eql.ai110.NOK.model.DeliveryMode;
import fr.eql.ai110.NOK.model.Item;
import fr.eql.ai110.NOK.model.PaymentMethod;
import fr.eql.ai110.NOK.service.CommandIBusiness;


@Controller(value="mbPurchase")
@Scope(value="session")
public class CommandManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Product> allProducts;
	private List<Product> allProductsByCategory;
	private Product productSelected; 
	
	private Customer customer; 
	
	private Cart cart = new Cart(); 
	
	private Integer quantity;
	
	private Category category;
	private List<Category> allCategories; 

	private List<DeliveryMode> allDeliveryModes;
	private DeliveryMode selectOneDeliveryMode;
	
	private List<PaymentMethod> allPaymentMethod;
	private PaymentMethod selectOnePaymentMethod;
	
	List<Item> itemList = new ArrayList();
		
	List<ProductPurchase> commandLineList = new ArrayList();
	
	
	@Autowired
	private CommandIBusiness commandBusiness;

	
	
	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		customer = (Customer) session.getAttribute("connectedClient");
		
		allProducts = commandBusiness.getAllProducts();
		allCategories = commandBusiness.getAllCategories();
		allDeliveryModes = commandBusiness.getAllPurchaseMode();
		allPaymentMethod = commandBusiness.getAllPaymentMethod();
	}

	public String fetchAllProducts() {
		String forward = "product.xhtml?faces-redirection=true";
		return forward;
	}
	
	public String fetchProductsByCategory(Category categorySelected){
		category = categorySelected;
		allProductsByCategory = commandBusiness.getProductsByCategory(categorySelected);
		String forward = "categoryDetails.xhtml?faces-redirection=true";
		return forward;
	}
	
	public String submitDeliveryMode() {
		String forward = "product.xhtml?faces-redirection=true";
		return forward;
	}
	
	public String fetchProductDetails(Product product) {
		quantity=1;
		productSelected = product;
		String forward = "productDetails.xhtml?faces-redirection=true";
		return forward;
	}
	
	public String addToCart() {
		String forward = null;
		if(customer != null){
			Item item = new Item(); 
			item.setProduct(productSelected);
			item.setQuantity(quantity);
			item.setPrice(item.getProduct().getPrice() * item.getQuantity());
			itemList = cart.getItemList();
			Boolean isAlreadyOnCart = false; 
			for(Item i : itemList){
				if(i.getProduct().equals(productSelected)) {
					i.setQuantity(i.getQuantity()+quantity);
					i.setPrice(i.getProduct().getPrice() * i.getQuantity());
					isAlreadyOnCart = true;
				}
			}
			if(isAlreadyOnCart == false) {
				cart.addToItemList(item);
			}
			cart.calculateTotalAmount(itemList);
			forward = "index.xhtml?faces-redirection=true";
		}else {
			forward="login.xhtml?faces-redirection=true";
		}
		return forward;
	}
	
	public void updatePriceItem(Item item) {
		item.setPrice(item.getProduct().getPrice() * item.getQuantity());
		cart.calculateTotalAmount(itemList);
	}
	
	public void deleteItem(Item item) {
		itemList.remove(item);
		cart.calculateTotalAmount(itemList);
		System.out.println("montant total : " + cart.getTotalAmount());
	}
	
	public String validateCart() {
		String forward = null;
		if(customer != null){
			forward = "paiement.xhtml?faces-redirection=true";
		}else {
			forward = "login.xhtml?faces-redirection=true";
		}
		return forward;
	}
	
	public boolean CartNotEmpty() {
		return !cart.getItemList().isEmpty();
	}
	
	public boolean CartEmpty() {
		return cart.getItemList().isEmpty();
	}
	
	public String validatePurchase() {
		Purchase purchase = new Purchase(); 
		purchase.setDeliveryMode(selectOneDeliveryMode);
		purchase.setPaymentMethod(selectOnePaymentMethod);
		purchase.setCustomer(customer);
		purchase.setTotalAmount(cart.getTotalAmount());
		purchase.setPurchaseDate(LocalDate.now());
		commandBusiness.addOnePurchase(purchase);
		itemList = cart.getItemList();
		for(Item item: itemList) {
			ProductPurchase productPurchase = new ProductPurchase();  
			productPurchase.setQuantity(item.getQuantity());
			productPurchase.setProduct(item.getProduct());
			productPurchase.setPurchase(purchase);
			commandBusiness.addOneProductPurchase(productPurchase);
		}
		cart = new Cart();
		String forward = "index.xhtml?faces-redirection=true";
		return forward;
	}
	
	public List<Product> getAllProducts() {
		return allProducts;
	}

	public void setAllProducts(List<Product> allProducts) {
		this.allProducts = allProducts;
	}


	public List<Product> getAllProductsByCategory() {
		return allProductsByCategory;
	}

	public void setAllProductsByCategory(List<Product> allProductsByCategory) {
		this.allProductsByCategory = allProductsByCategory;
	}

	public List<Category> getAllCategories() {
		return allCategories;
	}

	public void setAllCategories(List<Category> allCategories) {
		this.allCategories = allCategories;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<DeliveryMode> getAllDeliveryModes() {
		return allDeliveryModes;
	}

	public void setAllDeliveryModes(List<DeliveryMode> allDeliveryModes) {
		this.allDeliveryModes = allDeliveryModes;
	}

	public DeliveryMode getSelectOneDeliveryMode() {
		return selectOneDeliveryMode;
	}

	public void setSelectOneDeliveryMode(DeliveryMode selectOneDeliveryMode) {
		this.selectOneDeliveryMode = selectOneDeliveryMode;
	}

	public Product getProductSelected() {
		return productSelected;
	}

	public void setProductSelected(Product productSelected) {
		this.productSelected = productSelected;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}

	public List<PaymentMethod> getAllPaymentMethod() {
		return allPaymentMethod;
	}

	public void setAllPaymentMethod(List<PaymentMethod> allPaymentMethod) {
		this.allPaymentMethod = allPaymentMethod;
	}

	public PaymentMethod getSelectOnePaymentMethod() {
		return selectOnePaymentMethod;
	}

	public void setSelectOnePaymentMethod(PaymentMethod selectOnePaymentMethod) {
		this.selectOnePaymentMethod = selectOnePaymentMethod;
	}



}
