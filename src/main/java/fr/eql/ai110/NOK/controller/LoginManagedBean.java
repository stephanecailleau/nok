package fr.eql.ai110.NOK.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.SessionAttributes;

import fr.eql.ai110.NOK.model.Customer;
import fr.eql.ai110.NOK.service.AccountIBusiness;


@Controller(value="mbLogin")
@Scope(value = "request")
public class LoginManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String email;
	private String password;
	private Customer customer;
	
	@Autowired
	private AccountIBusiness accountBusiness;
	
	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		customer = (Customer) session.getAttribute("connectedClient");
	}
	
	public String connexion() {
		String forward = "/login.xhtml?faces-redirection=true";
		return forward;
	}
	
	public String connect() {
		String forward = null;
		customer = accountBusiness.connect(email, password);
		if (customer != null) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
			session.setAttribute("connectedClient", customer);
			forward = "/index.xhtml?faces-redirection=true";
		} else {
			FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"Identifiant et/ou mot de passe incorrect(s)",
					"Identifiant et/ou mot de passe incorrect(s)"
					);
			FacesContext.getCurrentInstance().addMessage("loginForm:loginError", facesMessage);
			forward = "/login.xhtml?faces-redirection=false";
		}
		return forward;
	}

	public boolean isConnected() {
		return customer != null;
	}
	
	public boolean isDisconnected() {
		return customer == null;
	}
	
	public String disconnect() {
		HttpSession session = (HttpSession) FacesContext
				.getCurrentInstance()
				.getExternalContext()
				.getSession(true);
		session.invalidate();
		customer = null;
		return "/index.xhtml?faces-redirection=true";
	}
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public AccountIBusiness getAccountBusiness() {
		return accountBusiness;
	}

	public void setAccountBusiness(AccountIBusiness accountBusiness) {
		this.accountBusiness = accountBusiness;
	}


	
}
