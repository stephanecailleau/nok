package fr.eql.ai110.NOK.model;

import java.io.Serializable;
import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name="delivery_mode")
public class DeliveryMode implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="name")
	private String name;
	@OneToMany(mappedBy = "deliveryMode", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Purchase> purchases;
	
	
	public DeliveryMode(Integer id, String name, List<Purchase> purchases) {
		super();
		this.id = id;
		this.name = name;
		this.purchases = purchases;
	}

	public DeliveryMode() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Purchase> getPurchases() {
		return purchases;
	}

	public void setPurchases(List<Purchase> purchases) {
		this.purchases = purchases;
	}

	
	
	
}
