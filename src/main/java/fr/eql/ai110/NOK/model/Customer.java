package fr.eql.ai110.NOK.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="customer")
public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id; 
	@Column(name="civil_state") 
	private String civilState; 
	@Column(name="phone")
	private Integer phone; 
	@Column(name="lastname")
	private String lastname;
	@Column(name="firstname")
	private String firstname; 
	@Column(name="email")
	private String email; 
	@Column(name="password")
	private String password;
	@Column(name="registration_date")
	private Date registrationDate;
	@Column(name="unsubscription_date")
	private Date unsubscription_date;
	@Column(name="re_registration_date")
	private Date re_registrationDate;
	@Column(name="adress")
	private String adress;
	@Column(name="city")
	private String city;
	
	@OneToMany(mappedBy="customer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Purchase> purchases;

	public Customer(Integer id, String civilState, Integer phone, String lastname, String firstname, String email,
			String password, Date registrationDate, Date unsubscription_date, Date re_registrationDate, String adress,
			String city, Set<Purchase> purchases) {
		super();
		this.id = id;
		this.civilState = civilState;
		this.phone = phone;
		this.lastname = lastname;
		this.firstname = firstname;
		this.email = email;
		this.password = password;
		this.registrationDate = registrationDate;
		this.unsubscription_date = unsubscription_date;
		this.re_registrationDate = re_registrationDate;
		this.adress = adress;
		this.city = city;
		this.purchases = purchases;
	}

	public Customer() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCivilState() {
		return civilState;
	}

	public void setCivilState(String civilState) {
		this.civilState = civilState;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Date getUnsubscription_date() {
		return unsubscription_date;
	}

	public void setUnsubscription_date(Date unsubscription_date) {
		this.unsubscription_date = unsubscription_date;
	}

	public Date getRe_registrationDate() {
		return re_registrationDate;
	}

	public void setRe_registrationDate(Date re_registrationDate) {
		this.re_registrationDate = re_registrationDate;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Set<Purchase> getPurchases() {
		return purchases;
	}

	public void setPurchases(Set<Purchase> purchases) {
		this.purchases = purchases;
	}

	
	

}
