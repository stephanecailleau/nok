package fr.eql.ai110.NOK.model;

import java.util.ArrayList;
import java.util.List;



public class Cart {
	
	private List<Item> itemList = new ArrayList();

	private double totalAmount;

	public Cart(ArrayList<Item> itemList, double totalAmount) {
		super();
		this.itemList = itemList;
		this.totalAmount = totalAmount;
	}

	public void addToItemList(Item item) {
		this.itemList.add(item);
	}
	
	public void calculateTotalAmount(List<Item> itemList){
		totalAmount = 0.0;
		for(Item item : itemList) {
			this.totalAmount += item.getPrice();
		}
	}

	
	public Cart() {
		super();
	}

	
	
	public List<Item> getItemList() {
		return itemList;
	}

	public void setItemList(ArrayList<Item> itemList) {
		this.itemList = itemList;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	} 
	

	
}
