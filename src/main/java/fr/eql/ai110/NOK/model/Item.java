package fr.eql.ai110.NOK.model;

public class Item {

	private Product product; 
	private Integer quantity;
	private Float price;
	
	
	public Item(Product product, Integer quantity, Float price) {
		super();
		this.product = product;
		this.quantity = quantity;
		this.price = price;
	}


	public Item() {
		super();
	}


	public Product getProduct() {
		return product;
	}


	public void setProduct(Product product) {
		this.product = product;
	}


	public Integer getQuantity() {
		return quantity;
	}


	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}


	public Float getPrice() {
		return price;
	}


	public void setPrice(Float price) {
		this.price = price;
	}


	@Override
	public String toString() {
		return "Item [product=" + product.getName() + ", quantity=" + quantity + ", price=" + price + "]";
	} 


	
}
