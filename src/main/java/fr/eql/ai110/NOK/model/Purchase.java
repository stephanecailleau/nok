package fr.eql.ai110.NOK.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="purchase")
public class Purchase implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Customer customer;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private DeliveryMode deliveryMode;
	@OneToMany(mappedBy = "purchase", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<ProductPurchase> productPurchase;
	@Column(name="purchase_date")
	private LocalDate purchaseDate;
	@Column(name="purchase_cancellation_date")
	private LocalDate purchaseCancellationDate;
	@Column(name="purchase_modification_date")
	private LocalDate purchaseModificationDate;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private PaymentMethod paymentMethod;
	private double totalAmount; 
	
	
	

	public Purchase(Integer id, Customer customer, DeliveryMode deliveryMode, List<ProductPurchase> productPurchase,
			LocalDate purchaseDate, LocalDate purchaseCancellationDate, LocalDate purchaseModificationDate,
			PaymentMethod paymentMethod, double totalAmount) {
		super();
		this.id = id;
		this.customer = customer;
		this.deliveryMode = deliveryMode;
		this.productPurchase = productPurchase;
		this.purchaseDate = purchaseDate;
		this.purchaseCancellationDate = purchaseCancellationDate;
		this.purchaseModificationDate = purchaseModificationDate;
		this.paymentMethod = paymentMethod;
		this.totalAmount = totalAmount;
	}

	public Purchase() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public DeliveryMode getDeliveryMode() {
		return deliveryMode;
	}

	public void setDeliveryMode(DeliveryMode deliveryMode) {
		this.deliveryMode = deliveryMode;
	}

	public List<ProductPurchase> getProductPurchase() {
		return productPurchase;
	}

	public void setProductPurchase(List<ProductPurchase> productPurchase) {
		this.productPurchase = productPurchase;
	}

	public LocalDate getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(LocalDate purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public LocalDate getPurchaseCancellationDate() {
		return purchaseCancellationDate;
	}

	public void setPurchaseCancellationDate(LocalDate purchaseCancellationDate) {
		this.purchaseCancellationDate = purchaseCancellationDate;
	}

	public LocalDate getPurchaseModificationDate() {
		return purchaseModificationDate;
	}

	public void setPurchaseModificationDate(LocalDate purchaseModificationDate) {
		this.purchaseModificationDate = purchaseModificationDate;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	
	
	
	
}
