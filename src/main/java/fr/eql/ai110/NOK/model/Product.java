package fr.eql.ai110.NOK.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="name")
	private String name;
	@Column(name="price")
	private Float price;
	@OneToMany(mappedBy = "product", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<ProductPurchase> productPurchase;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Category category;
	@Column(name="energy")
	private Integer energy;
	@Column(name="protein")
	private Integer protein;
	@Column(name="carbohydrate")
	private Integer carbohydrate;
	@Column(name="fat")
	private Integer fat;
	@Column(name="salt")
	private Integer salt;
	@Column(name="picture")
	private String picture;
	@Column(name="description", columnDefinition="TEXT")
	private String description;
	
	public Product(Integer id, String name, Float price, List<ProductPurchase> productPurchase, Category category,
			Integer energy, Integer protein, Integer carbohydrate, Integer fat, Integer salt, String picture,
			String description) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.productPurchase = productPurchase;
		this.category = category;
		this.energy = energy;
		this.protein = protein;
		this.carbohydrate = carbohydrate;
		this.fat = fat;
		this.salt = salt;
		this.picture = picture;
		this.description = description;
	}

	public Product() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public List<ProductPurchase> getProductPurchase() {
		return productPurchase;
	}

	public void setProductPurchase(List<ProductPurchase> productPurchase) {
		this.productPurchase = productPurchase;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Integer getEnergy() {
		return energy;
	}

	public void setEnergy(Integer energy) {
		this.energy = energy;
	}

	public Integer getProtein() {
		return protein;
	}

	public void setProtein(Integer protein) {
		this.protein = protein;
	}

	public Integer getCarbohydrate() {
		return carbohydrate;
	}

	public void setCarbohydrate(Integer carbohydrate) {
		this.carbohydrate = carbohydrate;
	}

	public Integer getFat() {
		return fat;
	}

	public void setFat(Integer fat) {
		this.fat = fat;
	}

	public Integer getSalt() {
		return salt;
	}

	public void setSalt(Integer salt) {
		this.salt = salt;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		return Objects.equals(id, other.id);
	}

	
	
}
