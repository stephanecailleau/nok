
INSERT INTO `restaurant_db`.`category` (`id`, `name`) VALUES ('1', 'nouilles');
INSERT INTO `restaurant_db`.`category` (`id`, `name`) VALUES ('2', 'riz');
INSERT INTO `restaurant_db`.`category` (`id`, `name`) VALUES ('3', 'soupe');
INSERT INTO `restaurant_db`.`category` (`id`, `name`) VALUES ('4', 'salade');
INSERT INTO `restaurant_db`.`category` (`id`, `name`) VALUES ('5', 'curry');
INSERT INTO `restaurant_db`.`category` (`name`) VALUES ('boisson');
INSERT INTO `restaurant_db`.`category` (`name`) VALUES ('dessert');

INSERT INTO `restaurant_db`.`delivery_mode` (`name`) VALUES ('Click & Collect');
INSERT INTO `restaurant_db`.`delivery_mode` (`name`) VALUES ('Livraison');
INSERT INTO `restaurant_db`.`delivery_mode` (`name`) VALUES ('Sur place');


INSERT INTO `restaurant_db`.`customer` (`civil_state`, `email`, `firstname`, `lastname`, `password`, `phone`, `registration_date`) VALUES ('Mme', 'fifi@hotmail.fr', 'fabiene', 'Durant', 'abcd', '0160792798', '2022-02-10');
INSERT INTO `restaurant_db`.`customer` (`civil_state`, `email`, `firstname`, `lastname`, `password`, `phone`, `registration_date`) VALUES ('Mr', 'lolo@hotmail.fr', 'Laurent', 'Xep', 'abcd', '01235689', '2022-02-05');


INSERT INTO `restaurant_db`.`product` (`carbohydrate`, `description`, `energy`, `picture`, `fat`, `name`, `price`, `protein`, `salt`, `category_id`) VALUES ('15', 'Le pad thai  est un plat traditionnel thaïlandais à base de nouilles de riz, très apprécié et très consommé dans toute la Thaïlande', '676', 'padthai.png', '5.1', 'Pad Thai  ', '8', '7.3', '0.99', '1');
INSERT INTO `restaurant_db`.`product` (`carbohydrate`, `description`, `energy`, `picture`, `fat`, `name`, `price`, `protein`, `salt`, `category_id`) VALUES ('14', 'Pad See Ew est un plat de nouilles de riz succulent dont les sauces soya foncées et claires sont les vedettes avec le broccoli chinois et le poivre blanc', '750', 'padseeew.png', '6', 'Pad See Ew', '8.5', '7.5', '0.80', '1');
INSERT INTO `restaurant_db`.`product` (`carbohydrate`, `description`, `energy`, `picture`, `fat`, `name`, `price`, `protein`, `salt`, `category_id`) VALUES ('18', 'Kaeng Khiao Wan est une variété de curry de Thailande centrale à base de lait de coco et de piments verts.', '800', 'kaengkhiaowan.png', '6.5', 'Kaeng Khiao Wan', '9', '8', '0.9', '5');
INSERT INTO `restaurant_db`.`product` (`carbohydrate`, `description`, `energy`, `picture`, `fat`, `name`, `price`, `protein`, `salt`, `category_id`) VALUES ('17', 'Som Tam est une salade de papaye verte coupée en lamelles que l’on agrémente de sauce de poisson, de tomates, de citron, de cacahuètes et de petits piments très puissants', '780', 'somtam.png', '5', 'Som Tam', '8', '6', '1', '4');
INSERT INTO `restaurant_db`.`product` (`carbohydrate`, `description`, `energy`, `picture`, `fat`, `name`, `price`, `protein`, `salt`, `category_id`) VALUES ('16', 'Le Khao Phat est du riz frit avec des légumes. On l’accompagne généralement de viande, de poisson ou de tofu', '620', 'khaophat.png', '5.7', 'Khao Phat', '7.5', '7', '0.7', '2');
INSERT INTO `restaurant_db`.`product` (`carbohydrate`, `description`, `energy`, `picture`, `fat`, `name`, `price`, `protein`, `salt`, `category_id`) VALUES ('18', 'Cette soupe se mijote à partir de lait de coco que l’on agrémente de racine de galanga, de feuilles de combava, de feuilles de kaffir, de citronnelle et de coriandre. Au-delà des épices, on peut y mettre du poulet, des fruits de mer, du porc ou même du tofu', '700', 'tomkha.png', '6', 'Tom Kha', '8', '7.5', '1', '3');
INSERT INTO `restaurant_db`.`product` (`carbohydrate`, `description`,  `energy`, `picture`, `fat`, `name`, `price`, `protein`, `salt`, `category_id`) VALUES ('17', 'Panang est une pate de curry rouge aux saveurs d’arachide composé de piments secs, galanga, citronnelle, combava, coriandre, cumin, citron, d’ail et d’échalotes. Celui-ci se cuisine généralement dans du lait de coco dans lequel on ajoute de la viande, du poisson ou du tofu pour les végétariens! ', '750', 'panaeng.png', '6.5', 'Panaeng', '9', '8', '1.2', '5');
INSERT INTO `restaurant_db`.`product` (`carbohydrate`, `description`,  `energy`, `picture`, `fat`, `name`, `price`, `protein`, `salt`, `category_id`) VALUES ('18', 'Traditionnellement cuisiné à base de viande de porc, le Pat Krapao est un sauté parfumé au basilic thaï, rehaussé par du piment et une pointe de sauce d’huître. On le déguste avec du riz et parfois, on l’agrémente d’un œuf sur le plat', '800', 'padkrapao.png', '6.5', 'Pad Krapao', '8', '8', '1', '2');
INSERT INTO `restaurant_db`.`product` (`carbohydrate`, `description`, `energy`, `picture`, `fat`, `name`, `price`, `protein`, `salt`, `category_id`) VALUES ('18', 'C’est l’un des currys les plus doux que vous trouverez en Thaïlande. Ce plat se constitue de pommes de terre, d’oignons, de cacahuètes, de viande, poisson ou tofu. Le tout mijoté dans du lait de coco et de la pate de curry massaman.', '850', 'massaman.png', '7', 'Massaman', '9.5', '8', '1.1', '5');
INSERT INTO `restaurant_db`.`product` (`carbohydrate`, `description`, `energy`, `picture`, `fat`, `name`, `price`, `protein`, `salt`, `category_id`) VALUES ('16', 'Un bouillon de poulet infusé à la citronnelle sucrée, aigre et épicée rempli de nouilles de riz, de germes de soja, d\'oignon, de carotte, d\'oignon vert, de coriandre et de tomate avec votre choix de crevettes, boeuf, poulet, légumes, tofu ou poisson', '800', 'tomyum.png', '5.5', 'Tom Yum', '9', '6', '0.99', '3');
INSERT INTO `restaurant_db`.`product` (`carbohydrate`, `description`, `energy`, `picture`, `fat`, `name`, `price`, `protein`, `salt`, `category_id`) VALUES ('17', "Un bouillon de poulet thaïlandais parfumé rempli de nouilles de riz, de germes de soja d\'oignon, de carotte, d\'oignon vert, de coriandre, garni d\'échalote rouge frite croustillante avec votre choix de crevettes, boeuf, poulet, légumes, tofu ou poisson.", '850', 'noodlessoup.png', '6', 'Noodles Soup', '10', '7', '1.3', '3');
INSERT INTO `restaurant_db`.`product` (`description`, `name`, `picture`, `price`, `category_id`) VALUES ('Pas de pelure, pas de morceaux, juste le jus le plus doux et le plus savoureux. 4 oranges cueillies à la main dans chaque bouteille', 'Innocent Orange', 'boisson1.png', '3', '6');
INSERT INTO `restaurant_db`.`product` (`description`, `name`, `picture`, `price`, `category_id`) VALUES ('Jus au goyave, pomme et spiruline', 'Innocent Fa-Bu-Bleu', 'boisson2.png', '3', '6');
INSERT INTO `restaurant_db`.`product` (`description`, `name`, `picture`, `price`, `category_id`) VALUES ('Jus à la rubarbe, pomme et poire', 'Innocent Rubarbe', 'boisson3.png', '3', '6');
INSERT INTO `restaurant_db`.`product` (`description`, `name`, `picture`, `price`, `category_id`) VALUES ('Jus goyave, ananas et pomme ', 'Innocent Goyave Ananas', 'boisson4.png', '3', '6');
INSERT INTO `restaurant_db`.`product` (`description`, `name`, `picture`, `price`, `category_id`) VALUES ('Jus pomme, poire, comcombre', 'Innocent Green de Star', 'boisson5.png', '3', '6');
INSERT INTO `restaurant_db`.`product` (`description`, `name`, `picture`, `price`, `category_id`) VALUES ('Deux mochi crémeux au cheesecake fraise', 'Mochi Cheesecake Fraise ', 'dessert1.png', '4.5', '7');
INSERT INTO `restaurant_db`.`product` (`description`, `name`, `picture`, `price`, `category_id`) VALUES ('Deux mochi crémeux au chocolat', 'Mochi Chocolat', 'dessert2.png', '4.5', '7');



INSERT INTO `restaurant_db`.`payment_method` (`name`) VALUES ('Carte bancaire');
INSERT INTO `restaurant_db`.`payment_method` (`name`) VALUES ('Paypal');